#!/usr/bin/env node

const {dirname, resolve} = require('path');
const rootDir = dirname(dirname(__dirname))
const packageJson = require(`${rootDir}/package.json`);
const fs = require('fs');
const yaml = require('js-yaml');
const dotenv = require('dotenv');
const glob = require('glob');

function getFileConfigFromRawConfig(rawConfig){
    if(typeof rawConfig === 'string'){
        return {
            type: 'env',
            path: rawConfig
        };
    }else{
        return rawConfig;
    }
}

function getKeysFromEnvFile(filePath){
    return Object.keys(dotenv.parse(fs.readFileSync(resolve(filePath))));
}

function getKeysFromEnvK8s(filePath){
    const resource = yaml.load(fs.readFileSync(resolve(filePath)));
    return Object.keys(resource.data);
}

function getKeysFromFileConfig(fileConfig) {
    if(fileConfig.type === 'env'){
        return getKeysFromEnvFile(fileConfig.path)
    }

    if(fileConfig.type === 'k8s'){
        return getKeysFromEnvK8s(fileConfig.path);
    }
}

function red(text) {
    return '\033[31m' + text + '\033[0m';
}

function getFileConfigsFromFilePaths(type, filePaths) {
    const configs = [];
    for(const filePath of filePaths){
        configs.push({ type, path: filePath})
    }
    return configs;
}

function getFileConfigsFromRawConfig(rawConfig){
    const fileConfig = getFileConfigFromRawConfig(rawConfig);

    if(fileConfig.path.includes('*')) {
        return getFileConfigsFromFilePaths(fileConfig.type, glob.sync(fileConfig.path));
    }

    if(fs.statSync(fileConfig.path).isDirectory()) {
        const dirSep = fileConfig.path.substr(fileConfig.path.length - 1) === '/' ? '' : '/'
        return getFileConfigsFromFilePaths(fileConfig.type, glob.sync(`${fileConfig.path}${dirSep}*.env`));
    }

    return [fileConfig];
}

function configHasError(rawConfig, templateKeys){
    const fileConfigs = getFileConfigsFromRawConfig(rawConfig);

    let hasError = false;
    for(const fileConfig of fileConfigs){
        const result = fileConfigHasError(fileConfig, templateKeys);
        if(result){
            hasError = true;
        }
    }
    return hasError;
}

function fileConfigHasError(fileConfig, templateKeys) {
    const keys = getKeysFromFileConfig(fileConfig);

    const missingKeys = templateKeys.filter(x => !keys.includes(x));

    if(missingKeys.length > 0){
        console.log(red('Error. Missing environment variables'))
        console.log(red(`File: ${fileConfig.path}`));
        console.log(`Keys: \n  ${missingKeys.join('\n  ')}\n`)
        return true;
    } else{
        return false;
    }
}

function configItemHasError(config){
    const templateKeys = getKeysFromFileConfig(getFileConfigFromRawConfig(config.template));

    let hasError = false;
    for(const rawValidateConfig of config.validate){
        const result = configHasError(rawValidateConfig, templateKeys);
        if(result){
            hasError = true;
        }
    }
    return hasError;
}

function run() {
    let configsHaveError = false;

    for(const config of packageJson.envValidateConfigs) {
        const hasError = configItemHasError(config);
        if(hasError){
            configsHaveError = true;
        }
    }

    if(configsHaveError){
        process.exit(1);
    }
}

run();
