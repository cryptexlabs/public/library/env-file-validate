# Environment file validator

Have you ever added a new environment variable to your `.env` file only to realize after you tried to deploy to a higher
level environment that you forgot to add that new environment variable to the other environments?

This library will help you prevent that from happening.

## Description

Validates `.env` and other environment variable configuration files

## Installation

```
yarn add env-file-validate --dev
```

or
```
npm i env-file-validate --save-dev
```

## Configuration
Add the following to your `package.json`
```json
{
  "scripts": {
    "env-file-validate": "env-file-validate",
  },
  "envValidateConfigs": [
    {
      "template": ".env",
      "validate": [
        "../docker/.env",
        "../k8s/aws-sns/env/*.env"
      ]
    },
    {
      "template": "secrets.env",
      "validate": [
        "../docker/secrets.env",
        {
          "type": "k8s",
          "path": "../k8s/aws-sns/templates/env-from-secrets.yaml"
        }
      ]
    }
  ]
}
```

### Options
| Property | Description | Required | Example |
| :--- | :--- | :---: | :--- |
| type   | The type of env file it is. Default is 'env' if only path is provided. | true | k8s |
| path   | The relative path to the file(s). Can include a wildcard | true | ../k8s/aws-sns/templates/env-from-secrets.yaml |

## Run

```bash
yarn env-file-validate
```

or

```bash
npm run env-file-validate
```

## Troubleshooting

### Helm templates

If you are parsing a helm file be sure to put quotes around your values. 
```yaml
data:
  MY_VAR: "{{ .Values.myvar }}"
```

If you have lines that are just declaring variables put a comment mark `#` in front of it so that it is ignored by `js-yaml
```yaml
#{{ $mySpecialVar := .Values.myvar }}
```

### Kubernetes type
`k8s` type only works with `ConfigMap` and `Secret` resource types. It assumes there is a key value pair map under the `data` attribute.

### Yaml files
You can only have one document per file. Separating documents in a single file like this:
```yaml
document1:
---
document2:
```
is not supported

## License
MIT

## Contributors
- Josh Woodcock